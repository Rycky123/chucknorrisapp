package com.example.chucknorris.model.domain

import com.example.chucknorris.model.ChuckRepository
import com.example.chucknorris.model.categories
import com.example.chucknorris.viewModel.ChuckViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class InteractorTest{

    @RelaxedMockK
    @MockK
    private  lateinit var chuckRepository: ChuckRepository
    private  lateinit var getInteractor: Interactor
   // private  lateinit var getModel: ChuckViewModel
    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        getInteractor = Interactor(chuckRepository)
    }


    @Test
    fun whenApiReturnData() = runBlocking {
        //Given
        val list = categories()
        coEvery { getInteractor.dataCategories()} returns list
        // when
       var res = getInteractor.dataCategories()
        //Then
        coVerify (exactly = 1) { chuckRepository.getCategoriesChuck() }
        assert(list == res)

    }
}
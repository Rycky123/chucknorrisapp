package com.example.chucknorris.view

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.chucknorris.R
import com.example.chucknorris.databinding.FragmentDetailsBinding
import com.example.chucknorris.databinding.QueryViewBinding
import com.example.chucknorris.model.Result
import com.example.chucknorris.model.data.Loading
import com.example.chucknorris.view.HomeFragment.Companion.categoriesGlob
import com.example.chucknorris.viewModel.ChuckViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment() {
    lateinit var binding: FragmentDetailsBinding
    val mvvm: ChuckViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var loading = Loading(requireActivity(), R.layout.loading_screen)
        loading.startL()
        var handler = Handler()
        handler.postDelayed(object :Runnable{
            override fun run() {
                loading.isDismiss()
            }
        }, 1200)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mvvm.ShowInformation(categoriesGlob)
        getDataTopic()
        Log.e("Details", categoriesGlob)

        binding.btnBack.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_detailsFragment_to_homeFragment)
        }

        binding.btnQuery.setOnClickListener {
            mvvm.ShowQuery(categoriesGlob)
            getDataQuery()
        }
    }

    fun getDataTopic(){
        mvvm.ObserverInformation().observe(viewLifecycleOwner, Observer { inf ->
            binding.textDetailsValue.text = inf.value
            binding.textDetailsTime.text = inf.created_at
            binding.textDetailsUpd.text = inf.icon_url
            binding.textDetails.text = inf.id

            Glide.with(requireActivity()).load(inf.icon_url).into(binding.img)
        })
    }

    fun getDataQuery(){
        mvvm.LiveDataQuery.observe(viewLifecycleOwner, Observer { data ->

           AlertTest(data)
        })
    }


    fun AlertTest(data: List<Result>){
        val mDialog = LayoutInflater.from(context).inflate(R.layout.query_view,null)
        val mBuilder =androidx.appcompat.app.AlertDialog.Builder(requireContext()).setView(mDialog)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mAlertDialog.setCancelable(false)
        val bindingDialog =QueryViewBinding.bind(mDialog)

        bindingDialog.btnclose.setOnClickListener {
            mAlertDialog.dismiss()
        }

        data.forEach {
            bindingDialog.textQuery.text = it.toString()
        }

    }



}
package com.example.chucknorris.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.example.chucknorris.R
import com.example.chucknorris.databinding.FragmentHomeBinding
import com.example.chucknorris.model.adapter.adapterCategories
import com.example.chucknorris.model.data.Loading
import com.example.chucknorris.viewModel.ChuckViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding
    val mvvm: ChuckViewModel by viewModels()
    var categorias = ArrayList<String>()
    var adapterCat = adapterCategories(onClickListener = {pos -> Topics(pos)})
//    var loading = Loading(requireActivity(), R.layout.loading_screen)
    companion object{
        var categoriesGlob = ""
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        getDataCategories()
        mvvm.ShowCategories()
        initRecicler()

    }

    fun getDataCategories(){
        mvvm.ObserverCategories().observe(viewLifecycleOwner, Observer { cat ->
            categorias.clear()
            cat.forEach {
                categorias.add(it)
            }

            adapterCat.setCategories(categorias)

        })
    }

    fun initRecicler(){
        binding.recyclerCategories.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = adapterCat
        }
    }



    fun Topics(cat: String){
        Navigation.findNavController(binding.root).navigate(R.id.action_homeFragment_to_detailsFragment)
        categoriesGlob = cat


    }



}
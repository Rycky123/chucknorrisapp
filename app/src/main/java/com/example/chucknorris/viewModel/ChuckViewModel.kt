package com.example.chucknorris.viewModel


import androidx.lifecycle.*
import com.example.chucknorris.model.domain.Interactor
import com.example.chucknorris.model.Prueba
import com.example.chucknorris.model.Result
import com.example.chucknorris.model.categories
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChuckViewModel @Inject constructor(
    var interactor: Interactor
) :ViewModel() {


    var LiveDataCategories = MutableLiveData<categories>()
    var LiveDataInformation = MutableLiveData<Prueba>()
    var LiveDataQuery = MutableLiveData<List<Result>>()

    fun ObserverCategories():LiveData<categories>{
        return LiveDataCategories
    }

    fun ObserverInformation():LiveData<Prueba>{
        return LiveDataInformation
    }

    fun ObserverQuery():LiveData<List<Result>>{
        return LiveDataQuery
    }

    fun ShowCategories(){
        viewModelScope.launch {
         var result = interactor.dataCategories()
            if (!result.isEmpty()){
            LiveDataCategories.postValue(result)
            }
        }
    }

    fun ShowInformation(cat: String){
        viewModelScope.launch {

            var result = interactor.DataInformation(cat)

            if (result != null ){
                LiveDataInformation.postValue(result)
            }
        }
    }

    fun ShowQuery(query:String){
        viewModelScope.launch {
            var result = interactor.dataQuery(query)
            if (!result.isEmpty()){
                LiveDataQuery.postValue(result)
            }
        }
    }



}
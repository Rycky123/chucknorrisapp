package com.example.chucknorris.model.data

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitInstance {

  fun providerRetofit():Retrofit{
      return Retrofit.Builder()
          .baseUrl("https://api.chucknorris.io/jokes/")
          .addConverterFactory(GsonConverterFactory.create())
          .build()
  }

}
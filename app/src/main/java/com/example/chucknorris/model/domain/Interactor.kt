package com.example.chucknorris.model.domain

import android.util.Log
import com.example.chucknorris.model.*
import javax.inject.Inject

class Interactor @Inject constructor(var repository: ChuckRepository) {


    suspend fun dataCategories():categories{
       var result = repository.getCategoriesChuck()
        if (result.body() != null){
            return result.body()!!
        }
        return categories()
    }


    suspend fun DataInformation(cat: String) : Prueba{
        var call = repository.getMoreInformation(cat).body()
        Log.e("Data", call.toString())
         return call!!
    }

    suspend fun dataQuery(query:String): List<Result>{
        var call = repository.CallQuery(query).body()!!.result
        Log.e("DataResul", call.toString())
        return call
    }


}
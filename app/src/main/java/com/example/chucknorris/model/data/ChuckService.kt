package com.example.chucknorris.model.data

import com.example.chucknorris.model.Prueba
import com.example.chucknorris.model.busqueda
import com.example.chucknorris.model.categories
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class ChuckService @Inject constructor(
    val retrofit: ApiService
) {

    suspend fun getServiceChuckCategories() : Response<categories> {
        return withContext(Dispatchers.IO){
            val response = retrofit.getCategories()
            response
        }

    }

    suspend fun getServiceChuckInformation(category:String) : Response<Prueba> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.getInformation(category)
            response
        }
    }


    suspend fun getServiceQuery(query:String) : Response<busqueda>{
        return withContext(Dispatchers.IO){
            val response = retrofit.getQuery(query)
            response
        }
    }
}
package com.example.chucknorris.model.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.chucknorris.R
import com.example.chucknorris.databinding.ItemCategoriesBinding

class adapterCategories(val onClickListener:(String) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var listCategories =ArrayList<String>()

    @SuppressLint("NotifyDataSetChanged")
    fun setCategories(category: ArrayList<String>){
        this.listCategories = category
        notifyDataSetChanged()
    }

    class ViewCategory(view:View): RecyclerView.ViewHolder(view){
        var binding = ItemCategoriesBinding.bind(view)
        fun render(listC: String){
            binding.textCategory.text = listC
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       val layoutInflater = LayoutInflater.from(parent.context)
        var view = layoutInflater.inflate(R.layout.item_categories, parent, false)
        return ViewCategory(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var viewholder: ViewCategory = holder as ViewCategory
        var item = listCategories[position]
        viewholder.render(item)

        viewholder.itemView.setOnClickListener {
        onClickListener(item)
        }
    }

    override fun getItemCount(): Int {
        return listCategories.size
    }
}
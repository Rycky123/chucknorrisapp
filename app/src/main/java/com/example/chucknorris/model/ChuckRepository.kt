package com.example.chucknorris.model

import android.app.DownloadManager
import android.util.Log
import com.example.chucknorris.model.data.ApiService
import com.example.chucknorris.model.data.ChuckService
import retrofit2.Response
import javax.inject.Inject

class ChuckRepository @Inject constructor(
    var api: ChuckService
) {

   suspend fun getCategoriesChuck() : Response<categories>{
        var call = api.getServiceChuckCategories()
        return call

    }

   suspend fun getMoreInformation(category:String): Response<Prueba>{
         var call = api.getServiceChuckInformation(category)
       Log.e("call", call.toString())
        return call
    }

    suspend fun CallQuery(query: String): Response<busqueda>{
        var call = api.getServiceQuery(query)
        Log.e("callQuery", call.toString())
        return call
    }


}
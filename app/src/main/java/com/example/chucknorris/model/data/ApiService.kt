package com.example.chucknorris.model.data

import com.example.chucknorris.model.Prueba
import com.example.chucknorris.model.busqueda
import com.example.chucknorris.model.categories

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("categories")
    suspend fun getCategories(): Response<categories>

    @GET("random?")
    suspend fun getInformation(@Query("category")CatNam:String): Response<Prueba>

    @GET("search?")
    suspend fun getQuery(@Query("query")q:String): Response<busqueda>

}
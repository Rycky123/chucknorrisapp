package com.example.chucknorris.model.data

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable


class Loading (val mActiv: Activity, val view:Int){


    private lateinit var isdialog:AlertDialog

    fun startL(){
        var inflate = mActiv.layoutInflater
        val dialogView = inflate.inflate(view, null)

        val bul = AlertDialog.Builder(mActiv).setView(dialogView)
         isdialog = bul.show()
        isdialog.setCancelable(false)
        isdialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    }

    fun isDismiss(){
        isdialog.dismiss()

    }
}